<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/1', function () {
    return view('lesson1');
});
Route::get('/2', function () {
    return view('lesson2');
});
Route::get('/3', function () {
    return view('lesson3');
});
Route::get('/4', function () {
    return view('lesson4');
});
Route::get('/5', function () {
    return view('lesson5');
});
Route::get('/6', function () {
    return view('lesson6');
});
Route::get('/7', function () {
    return view('lesson7');
});
Route::get('/8', function () {
    return view('lesson8');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});
