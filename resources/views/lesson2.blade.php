<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
        </style>
    </head>
    <body>


        <div class="container">
            <div id="app" class="content">

                <span class="error" v-show="!message">
                    You must enter a message
                </span>
                <br />
                <textarea v-model="message"></textarea>
                <br />

                <button type="submit" v-if="message">
                    Send Message
                </button>

                <pre>
                @{{ $data | json }}
                </pre>
            </div>
        </div>



        <script src="http://cdnjs.cloudflare.com/ajax/libs/vue/1.0.16/vue.min.js"></script>

        <script>
            new Vue({
                el: '#app',
                data: {
                    message: ''
                }
            });
        </script>


    </body>
</html>
