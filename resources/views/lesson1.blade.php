<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
        </style>
    </head>
    <body>


        <div class="container">
            <div id="app" class="content">

                <h1>@{{ message }}</h1>

                <input v-model="message">


<pre>
@{{ $data | json }}
</pre>

            </div>
        </div>


        <script src="http://cdnjs.cloudflare.com/ajax/libs/vue/1.0.16/vue.min.js"></script>

        <script>
            var data = {
                message: 'Hello World'
            };
            new Vue({
                el: '#app',
                data: data
            });
        </script>


    </body>
</html>
