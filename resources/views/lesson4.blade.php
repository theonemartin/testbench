<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
        </style>
    </head>
    <body>


        <div class="container">
            <div id="app" class="content">

                {{--<input type="color">--}}




                <my-counter heading="Likes"></my-counter>
                <my-counter heading="Dislikes"></my-counter>

            </div>
        </div>

        <template id="counter-template">
            <h1>@{{ heading }}</h1>
            <button @click="count += 1">@{{ count }}</button>
        </template>

        <script src="http://cdnjs.cloudflare.com/ajax/libs/vue/1.0.16/vue.js"></script>

        <script>
            Vue.component('my-counter', {
                template: '#counter-template',
                props: [ 'heading' ],
                data: function () {
                    return { count: 0 };
                }
            });

            new Vue({
                el: '#app'
            });
        </script>


    </body>
</html>
