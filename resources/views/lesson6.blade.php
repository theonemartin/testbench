<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                /*font-weight: 100;*/
                font-family: 'Lato';
            }

            .container {
                /*text-align: center;*/
                display: table-cell;
                vertical-align: middle;
                font-family: "Arial Black", arial-black;
                font-size: 1em;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
        </style>
    </head>
    <body>


        <div id="app" class="container">
{{--<pre>            @{{ $data | json }}--}}
{{--</pre>--}}

                <div v-for="plan in plans">
                    <plan :plan="plan" :active.sync="active"></plan>
                </div>






        </div>


        <template id="plan-template">
{{--            @{{ $data | json }}--}}
            <div>
                <spam>@{{ plan.name }}</spam>
                <spam>@{{ plan.price }} / month</spam>

                <button v-show="this.active.name !== plan.name" @click="setActivePlan">@{{ isUpgrade ? 'Upgrade' : 'Downgrade' }}</button>

                <button v-else>Current</button>
            </div>
        </template>






        <script src="http://cdnjs.cloudflare.com/ajax/libs/vue/1.0.16/vue.js"></script>

        <script>
            Vue.component('plan', {
                template: '#plan-template',
                props: ['plan', 'active'],
                methods: {
                    setActivePlan: function() {
                        this.active = this.plan;
                    }
                },
                computed: {
                    isUpgrade: function () {
                        return this.plan.price > this.active.price;
                    }
                }
            });

            new Vue({
                el: '#app',
                data: {
                    plans: [
                        { name: 'Enterprise', price: 100 },
                        { name: 'Pro', price: 50 },
                        { name: 'Personal', price: 10 },
                        { name: 'Free', price: 0 }
                    ],
                    active: {}
                }
            });
        </script>


    </body>
</html>
