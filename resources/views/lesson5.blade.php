<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
        </style>
    </head>
    <body>


        <div class="container">
            <div class="content">

                <h1>Skill: @{{ skill }}</h1>

                <br />

                <input type="text"  v-model="points">

                <br />
                <hr />
                <br />

                <h1> @{{ first }} @{{ last }}</h1>
                <h1> @{{ fullName }}</h1>

                <input type="text" v-model="first" placeholder="First Name">
                <input type="text" v-model="last" placeholder="Last Name">
            </div>
        </div>







        <script src="http://cdnjs.cloudflare.com/ajax/libs/vue/1.0.16/vue.js"></script>

        <script>


            new Vue({
                el: 'body',
                data: {
                    points: 300,
                    first: 'Brianna',
                    last: 'Martin',
                    fullName: 'Brianna Martin'
                },

                computed: {
                    skill: function () {
                        if (this.points <= 100) {
                            return 'Beginner';
                        }
                        return 'Intermediate';
                    },
                    fullName: function() {
                        return this.first + ' ' + this.last;
                    }
                }
//                ,
//
//                watch: {
//                    first: function (first) {
//                        this.fullName = first + ' ' + this.last;
//                    },
//                    last: function (last) {
//                        this.fullName = this.first + ' ' + last;
//                    }
//                }
            });
        </script>


    </body>
</html>
